import { useState } from "react";

export const useForm = (initialform = {}) => {

    const [formState, setFormState] = useState(initialform)

    const onInputChange = ({ target }) => {
        const {value, name} = target;
        setFormState({
            ...formState,
            [name]: value
        })
    }

    const onResetForm = () => {
        setFormState(initialform)
    }

    return {
        formState, onInputChange, onResetForm, ...formState
    } 
}
