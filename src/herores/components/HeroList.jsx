import { useMemo } from "react";
import { getHeroresByPublisher } from "../helpers";
import { HeroCard } from "./";


export const HeroList = ({ publisher }) => {
const heroes =  useMemo(() => getHeroresByPublisher(publisher));
  return (
    <div className="row rows-cols-1 row-cols-md-3 g-3">
        {
            heroes.map( h => (
                <HeroCard key={h.id} hero={h}/>
            ))
        }
    </div>
  )
}
