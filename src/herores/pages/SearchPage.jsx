import { useLocation, useNavigate } from 'react-router-dom';
import { HeroCard } from '../components';
import { useForm } from '../hooks/useForm';
import queryString from 'query-string';
import { getHeroesByName } from '../helpers';
import { useMemo } from 'react';


const DisplayAlert = ({ stringSearch, countHeroes }) => {
    let ClassCustom = 'animate__animated animate__fadeIn';
    return (stringSearch === '') ? <div className={`alert alert-primary ${ClassCustom}`}>Search a hero</div>
    :(countHeroes === 0) && <div className={`alert alert-danger ${ClassCustom}`}>No hero with <b>{ stringSearch }</b></div>;
};

export const SearchPage = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const { q = '' } = queryString.parse( location.search );
    // console.log(location);
    // console.log(query);
    const heroes =  useMemo(() => getHeroesByName(q));

    const { onInputChange, searchText } = useForm({
        searchText: q,
    });
    
    const onSearchSubmit = (e) => {
        e.preventDefault();
        // if(searchText.trim().length <= 1) return;
        
        navigate(`?q=${ searchText.toLowerCase().trim() }`);
    };
    
    return (
        <>
            <h1>Search</h1>
            <hr />
            <div className="row">
                <div className="col-5">
                    <h4>Searching</h4>
                    <hr />
                    <form onSubmit={onSearchSubmit}>
                        <input
                            type="text"
                            placeholder="Search a hero"
                            className="form-control"
                            name="searchText"
                            autoComplete="off"
                            value={searchText}
                            onChange={onInputChange}
                        />
                        <button
                            className="btn btn-outline-primary mt-1"
                        >Search</button>
                    </form>
                </div>
                <div className="col-7">
                    <h4>Results</h4>
                    <hr />

                    <DisplayAlert stringSearch={q} countHeroes={heroes.length}/>
                    
                    {
                        heroes.map( hero => (
                            <HeroCard hero={hero} key={hero.id} />
                        ))
                    }
                </div>
            </div>
        </>
    )
}
