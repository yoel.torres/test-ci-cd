import { types } from "../../../auth/types/type";

describe('Pruebas sobre los types', () => {
    test('debe de regresar estos types', () => {
        expect(types).toEqual({
            login: '[Auth] Login',
            logout: '[Auth] Logout',
        });
    });
});