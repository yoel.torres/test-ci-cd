import { useReducer } from "react";
import { authReducer } from "../../../auth/context/authReducer";
import { types } from "../../../auth/types/type";

describe('Pruebas de authReducer', () => {
    test('debe de retornar el estado por defecto', () => {
        const state = authReducer({ logged: false }, {});
        expect(state).toEqual({ logged: false });
    });

    test('debe de llamar al login autenticar y establecer el user', () => {
        const state = authReducer({ logged: false }, { type: types.login, payload: { id: 999, name: 'Test' } });
        expect(state.user).toEqual({ id: 999, name: 'Test' });
        expect(state.logged).toBe(true);
    });

    test('debe de borrar el name del usuario y logged en false', () => {
        const state = authReducer({ logged: true, user: { id: 999, name: 'Test' }}, { type: types.logout });
        expect(state.user).toEqual(undefined);
        expect(state.logged).toBe(false);
    });
});