import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { Authcontext } from "../../auth";
import { PrivateRoute } from "../../router/PrivateRoute";

describe('Pruebas en <PrivateRoute />', () => {
    test('debe de mostrar el children si está autenticado', () => {

        Storage.prototype.setItem = jest.fn();

        const contextValue = {
            logged: true,
            user: { id: 'ABC', name: 'Juan Carlos' }
        };

        render(
            <Authcontext.Provider value={contextValue}>
                <MemoryRouter>
                    <PrivateRoute>
                        <h1>Ruta privada</h1>
                    </PrivateRoute>
                </MemoryRouter>
            </Authcontext.Provider>
        );

        expect(screen.getByText('Ruta privada')).toBeTruthy();
        expect( localStorage.setItem ).toHaveBeenCalled();
        expect( localStorage.setItem ).toHaveBeenCalledWith('lastPath', '/');
    });

});