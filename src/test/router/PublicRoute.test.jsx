import { render, screen } from "@testing-library/react";
import { MemoryRouter, Route, Routes } from "react-router-dom";
import { Authcontext } from "../../auth/context/Authcontext";
import { PublicRoute } from "../../router/PublicRoute";

describe('Pruebas en <PublicRoute />', () => {

    test('debe de mostrar el children si no esta autenticado', () => {

        const contextValue = {
            logged: false
        };

        render(
            <Authcontext.Provider value={contextValue}>
                <PublicRoute>
                    <h1>RUTA PÚBLICA</h1>
                </PublicRoute>
            </Authcontext.Provider>
        );

        expect(screen.getByText('RUTA PÚBLICA')).toBeTruthy();
    });

    test('debe de navegar si esta autenticado', () => {
        const contextValue = {
            logged: true,
            user: { name: 'Strider', id: 'ABC123' }
        };

        render(
            <Authcontext.Provider value={contextValue}>
                <MemoryRouter initialEntries={['/login']}>
                    <Routes>
                        <Route path='login' element={
                            <PublicRoute>
                                <h1>RUTA PÚBLICA</h1>
                            </PublicRoute>
                        } />
                        <Route path='marvel' element={<h1>Página Marvel</h1>} />
                    </Routes>
                </MemoryRouter>
            </Authcontext.Provider>
        );
        expect(screen.getByText('Página Marvel')).toBeTruthy();
    });
});