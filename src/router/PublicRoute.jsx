import { useContext } from "react";
import { Navigate } from "react-router-dom";
import { Authcontext } from "../auth";

export const PublicRoute = ({ children }) => {
  const { logged } = useContext(Authcontext);
  return (!logged) ? children : <Navigate to="/marvel" />;
}
