import { useReducer } from "react"
import { types } from "../types/type";
import { Authcontext } from "./Authcontext"
import { authReducer } from "./authReducer";

const init = () => {
  const user = JSON.parse(localStorage.getItem('user'));

  return {
    logged: !!user,
    user: user
  }
};

export const AuthProvider = ({ children }) => {

  const [authState, dispatch] = useReducer(authReducer, {}, init);

  const login = async (name = '') => {
    const user = { id: 1, name };
    const action = { type: types.login, payload: user };

    localStorage.setItem('user', JSON.stringify(user));

    dispatch(action);
  };

  const logout = () => {
    localStorage.removeItem('user');
    const action = { type: types.logout };

    dispatch(action);
  };


  return (
    <Authcontext.Provider value={{ ...authState, login, logout }}> {/* Todo esto se crea con el createContext() => Authcontext.jsx */}
      {children}
    </Authcontext.Provider>
  )
}
